import java.io.*;
import java.util.*;


//  ZEYNEP K烿E
public class SortingClass {

    private static int[] heap_a;
    private static int heap_n;
    private static int heap_left;
    private static int heap_right;
    private static int heap_largest;
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] ar1 = generateRndArray(1, 1000);
		int[] ar2 = generateEqualArray(1, 1000);
		int[] ar3 = generateIncArray(1, 1000);
		int[] ar4 = generateDecArray(1, 1000);
		
		int[] ar5 = generateRndArray(1, 10000);
		int[] ar6 = generateEqualArray(1, 10000);
		int[] ar7 = generateIncArray(1, 10000);
		int[] ar8 = generateDecArray(1, 10000);
		
		int[] ar9  = generateRndArray(1, 100000);
		int[] ar10 = generateEqualArray(1, 100000);
		int[] ar11 = generateIncArray(1, 100000);
		int[] ar12 = generateDecArray(1, 100000);
		

			 // A single line must be active for the same array.  (Ayn覺 dizi i癟in tek bir sat覺r aktif olmal覺.癟羹nk羹 ilk sort i��leminde dizi do��ru s覺raya girerse random s覺ral覺 olmaz.) 
			 System.out.println(sortingArrays(ar1,"heap"));
			//System.out.println(sortingArrays(ar2,"quick"));
			 //System.out.println(sortingArrays(ar4,"shell"));


		 

	}
	
	public static String sortingArrays(int[] ar,String str ) {
		String result="";
		long startTime;
		long estimatedTime; 
        switch (str) {
        case "heap" :
    		startTime = System.nanoTime();
    		heapSort(ar);
    		estimatedTime = System.nanoTime() - startTime;
    		result=estimatedTime+"\n";
    		//result=Arrays.toString(ar); //array print..
    		return result;
        case "shell" :
    		startTime = System.nanoTime();
    		shellSort(ar);
    		estimatedTime = System.nanoTime() - startTime;
    		result=estimatedTime+"\n";
    		return result;
        case "quick" :
    		startTime = System.nanoTime();
    		dualPivotQuickSort(ar);
    		estimatedTime = System.nanoTime() - startTime;
    		result=estimatedTime+"\n";
    		return result;
        default :
            System.out.println("Wrong choose.");
            break;
        }
        return result;

	}
	
	///////                                                   GENERATE ARRAY                                                           ///////////7
	// Generate Random Array
	public static int[] generateRndArray(int a, int b) {
		Random gen = new Random();

		int size = b - a + 1;
		int[] array = new int[size];

		for (int i = 0; i < size; i++)
			array[i] = gen.nextInt(1000000);

		return array;
	}
	// Generate Equal Array
	public static int[] generateEqualArray(int a, int b) {
		int size = b - a + 1;
		int[] array = new int[size];

		for (int i = 0; i < size; i++)
			array[i] = 1;

		return array;
	}
	// Generate Increasing Array
	public static int[] generateIncArray(int a, int b) {	
		int size = b - a + 1;
		int[] array = new int[size];

		for (int i = 0; i < size; i++)
			array[i] = i;

		return array;
	}
	// Generate Decreasing Array
	public static int[] generateDecArray(int a, int b) {
		int size = b - a + 1;
		int[] array = new int[size];

		for (int i = size; i < 0; i++)
			array[i] = i;

		return array;
	}
	
		
    //////                                                   HEAP SORT                                                                                        ////// 
	//Heap-Sort Function
	public static void heapSort(int[] arrayToSort) {
    	heap_a=arrayToSort;
    	buildheap(heap_a);
    	
    	for(int i=heap_n;i>0;i--){
    		exchangeHeap(0, i);
    		heap_n=heap_n-1;
    		maxheap(heap_a, 0);
    	}
	}
	
	
    // Build-Heap Function
    public static void buildheap(int []a){
    	heap_n=a.length-1;
    	for(int i=heap_n/2;i>=0;i--){
    		maxheap(a,i);
    	}
    }
    
    // Max-Heap Function
    public static void maxheap(int[] a, int i){
    	heap_left=2*i;
    	heap_right=2*i+1;
    	if(heap_left <= heap_n && a[heap_left] > a[i]){
    		heap_largest=heap_left;
    	}
    	else{
    		heap_largest=i;
    	}
    	
    	if(heap_right <= heap_n && a[heap_right] > a[heap_largest]){
    		heap_largest=heap_right;
    	}
    	if(heap_largest!=i){
    		exchangeHeap(i,heap_largest);
    		maxheap(a, heap_largest);
    	}
    }
    
    // exchangeHeap Function
    public static void exchangeHeap(int i, int j){
    	int t=heap_a[i];
    	heap_a[i]=heap_a[j];
    	heap_a[j]=t; 
    	}
    
    // Heap Sort Print Function
    public static void heapSortPrint(int[] a1) {
    	heapSort(a1);
    	for(int i=0;i<a1.length;i++){
    		System.out.print(a1[i] + " ");
    	}
    }

    //////                                               DUAL PIVOT QUICK SORT                                                                                //////   
	public static void dualPivotQuickSort(int[] arrayToSort) {
		Collections.shuffle(Arrays.asList(arrayToSort));
		dualPivotQuickSort(arrayToSort, 0, arrayToSort.length - 1);
		assert isQuickSorted(arrayToSort);
	}
	public static void dualPivotQuickSort(int[] arrayToSort, int low, int high) {
		if (high <= low) return;

		if (lessQuickSort(arrayToSort[low], arrayToSort[high])) exchangeQuickSort(arrayToSort, low, high);

		int lt = low + 1, gt = high - 1;
		int i = lt;

		while (i <= gt) {
			if (lessQuickSort(arrayToSort[i], arrayToSort[low])) exchangeQuickSort(arrayToSort, lt++, i++);
			else if (lessQuickSort(arrayToSort[high], arrayToSort[i])) exchangeQuickSort(arrayToSort, i, gt--);
			else i++;
		}

		exchangeQuickSort(arrayToSort, low, --lt);
		exchangeQuickSort(arrayToSort, high, ++gt);

		dualPivotQuickSort(arrayToSort, low, lt-1);
		if (lessQuickSort(arrayToSort[lt], arrayToSort[gt])) dualPivotQuickSort(arrayToSort, lt+1, gt-1);
		dualPivotQuickSort(arrayToSort, gt+1, high);

		assert isQuickSorted(arrayToSort, low, high);
	}
	public static boolean isQuickSorted(int[] ar) {
		return isQuickSorted(ar, 0, ar.length - 1);
	}

	public static boolean isQuickSorted(int[] ar, int low, int high) {
		for (int i = 0; i < ar.length; i++) 
			if (lessQuickSort(ar[i], ar[i-1])) return false;
		return true;
	}

	public static boolean lessQuickSort(Comparable a, Comparable b) {
		return a.compareTo(b) < 0;
	}

	public static void exchangeQuickSort(int[] ar, int a, int b) {
		int swap = ar[a];
		ar[a] = ar[b];
		ar[b] = swap;
	}
	
    //////                                                       SHELL SORT                                                                                ////// 	
		
	public static void shellSort(int[] arrayToSort) {
		int in, out, temp;
		int i = 1;

		while (i <= arrayToSort.length) {
			i = i * 2 + 1;
		}
		while (i > 0) {
			for (out = i; out < arrayToSort.length; out++) {
				temp = arrayToSort[out];
				in = out;

				while (in > i - 1 && arrayToSort[in - i] >= temp) {
					arrayToSort[in] = arrayToSort[in - i];
					in -= i;
				}
				arrayToSort[in] = temp;
			}
			i = (i - 1) / 2;

		}

	}
	
}
